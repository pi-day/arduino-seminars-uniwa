int value ;
int led = 10;

void setup() {
  // put your setup code here, to run once:
Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
value = analogRead(A0); //photoresistor on A0

Serial.println(value);//print the value
delay(200);//wait

if (value < 100){
  digitalWrite(led,1); //turn on the led
}
else{
  digitalWrite(led,0); //turn off the led
}

}
