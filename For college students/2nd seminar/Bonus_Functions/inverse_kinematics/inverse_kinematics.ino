/*
 * Άσκηση: Να γραφούν οι συναρτήσεις από την εικόνα στο φάκελο, για να υπολογίζονται
 * από το arduino. 
 * 
 * Οι συναρτήσεις της εικόνας ας γραφούν σε μορφή function. Δεν χρειάζεται να χρησι-
 * μοποιηθούν στο κώδικα.
 * 
 * Οι συναρτήσεις είναι από βραχίονα 3 βαθμών ελευθερίας, αντίστροφης κινηματικής.
 * Έχω φτιάξει ολόκληρο το βραχίονα:
 * https://gitlab.com/Basilisvirus/inverse-kinematics-3-dof-robotic-arm
 * 
 */

//================Libraries============

//for atan2(y,x)
#include <math.h> 

//=================Variables================
bool C3_Error = true;
double C3_Result;

//Arm lengths
float l1, l2, l3;

//============================Θ1===========================
double Th_1(int x,int y) {
   return Rad_To_Deg(atan2 (y,x)); //reverse x and y cause the library is reversed 
}

//============================C3===========================
double C3(int x,int y,int z)
{
  C3_Result = (sq(x)+sq(y)+sq(z-l1)-sq(l2)-sq(l3))/(2*l2*l3);
  
  //C3 has limitation due to square root. these limitations should not be exceeded
  if(C3_Result<(-1) || C3_Result>1)
  {
    C3_Error = true;
  }
  else{
    C3_Error=false;
    return C3_Result;
  }
}

//============================S3===========================
double S3(int x,int y,int z, bool Second_Solution=false)
{
  if (Second_Solution==false){//up elbow solution
      return sqrt(1-sq(C3(x,y,z)));
  }
  else if(Second_Solution==true){//down elbow solution
    return -sqrt(1-sq(C3(x,y,z)));
  }
}

//Atan2 returns Rad. We need Deg.
double Rad_To_Deg(double Conv){
  return Conv* 57.2957;
}

//============================Θ3===========================
double Th_3(int x,int y,int z,bool Second_Solution=false)
{
      return Rad_To_Deg(atan2(S3(x,y,z, Second_Solution),C3(x,y,z)));
}


//============================Θ2===========================
double Th_2(int x, int y, int z, bool Second_Solution=false)
{
 return Rad_To_Deg(atan2(z-l1, sqrt(sq(x)+ sq(y))) - atan2(l3*S3(x,y,z,Second_Solution), l2+(l3*C3(x,y,z)))); 
}




void setup() {
  // put your setup code here, to run once:

}

void loop() {
  // put your main code here, to run repeatedly:

}
