/*
 * Άσκηση: Να χρησιμοποιηθεί η σειριακή για να παίρνει συνεχόμενα είσοδο από τον χρήστη.
 * Η σειριακή μπορει να πάρει μόνο έναν χαρακτήρα κάθε φορά. 
 * 
 * Αν δωθεί ο αριθμός 2 να ανάβει το led. Αν δωθεί αριθμός 3 να σβήνει το led.
 * 
 * Το led να μην ανάβει κατευθείαν με τη τροφοδοσία του Arduino. Θεωρούμε ότι 
 * δε θέλουμε να ζορίσουμε το arduino αντλώντας ισχύ από τα pins του, οπότε χρησιμο-
 * ποιείται transistor για να ανάψει το led.
 * 
 * Δες το σχηματικό.
 * 
 */


//Pin variables
int led = 5; //led in pin 10

int serial =0;

void setup() {
  Serial.begin(9600);
  //Pin Modes
  pinMode(led, OUTPUT); //led pin outputs power
  //INIT
  digitalWrite(led, LOW); //initialize led with low

  Serial.println("give a number other than 0");
  Serial.println("number 2 turns on the led");
  
}

void loop() {

  Serial.println("===============");

  while(serial == -1 || serial == 0 || serial == 10){ //wait for any input except the two
    serial = Serial.read ();
  }
  
   Serial.println(serial, BIN); //print what you typed in Binary 
   
  if(serial == 50){ //serial makes '50' if you type 2
    digitalWrite(led,1);
  Serial.println("number 3 turns off the led");    
  }
  else if(51){ //serial makes '51' if you type 3
    digitalWrite(led,0);    
  }
  
  serial = 0; //make serial 0 to reset it.

}
