#define trigPin 13
#define echoPin 12
#define led 10


long duration, distance;

void setup() {
  Serial.begin (9600);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(led, OUTPUT);

}
void loop() {

  digitalWrite(trigPin, LOW);  
  delayMicroseconds(2); 
  digitalWrite(trigPin, HIGH);
  
//  delayMicroseconds(1000); 

  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distance = (duration/58);
  
  if (distance < 9) {  // This is where the LED On/Off happens
    digitalWrite(led,HIGH); // When the Red condition is met, the Green LED should turn off
}
  else {
    digitalWrite(led,LOW);
  }

    Serial.print(distance);
    Serial.println(" cm");
  delay(500);
    }
