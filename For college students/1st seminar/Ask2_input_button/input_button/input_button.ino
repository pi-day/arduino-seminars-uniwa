/*
 * Άσκηση: Να Πατάμε ένα button, να ανάβει το led, όταν ξανα-πατήσουμε το button, να
 * σβήνει. 
 * Κάθε φορά που πατάμε το button, το led αλλάζει κατάσταση.
 * 
 * Η συνδεσμολογία φαίνεται στην εικόνα.
 * Ένα τέτοιο tutorial στα ελληνικά:
 * https://www.youtube.com/watch?v=J0iPRYAxnf8
 * 
 * Το button είναι βραχικυκλωμένο τα απέναντι pins του.
 * Μελετάμε: Τι σημαίνει Pull-up resistor.
 */


//Pin variables
int led = 10; //led in pin 10
int button = 11; //button in pin 11

//Variables
int prevStateLed = 1; //remember the previous state of led, default = 0 off
int buttonRead = 1; //default button reads 1 (pull up resistor)

void setup() {
  
pinMode(led, OUTPUT); //led pin outputs power
pinMode(button, INPUT); //button inputs signal

digitalWrite(led, HIGH); //initialize led with low
}

void loop() {
  buttonRead = digitalRead(button); //read the button state. If pressed, buttonRead = 0;

  if (buttonRead == 0)//if i press the button
  {
    
    if(prevStateLed == 1)//if the led was on,
      {
        digitalWrite(led, LOW); //turn off the led
        prevStateLed = 0; //now, led is 0
      }
      
    else if(prevStateLed == 0)//if the button was off,
      {
       digitalWrite(led, HIGH); //turn on the led
        prevStateLed = 1;        //now led is 1 (on)
      }

 delay(200);    
  }//if button pressed


}
