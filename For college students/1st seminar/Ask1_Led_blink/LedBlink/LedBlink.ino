/*
 * 
  Turns an LED on for one second, then off for one second, repeatedly at pin 10

  Υπάρχουν απειρα παραδείγματα του κώδικα online, και είναι ο πρώτος κώδικας για να
  ξεκινήσεις με μικροελεγκτές. Δες:
  https://www.youtube.com/watch?v=62MGqPIyoDg
*/
int led = 10; //declare variable led

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital led pin as an output.
  pinMode(led, OUTPUT); //same as pinMode(10,OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(led, HIGH);   // turn the LED on ,same as digitalWrite(10, HIGH);
  delay(100);                       // wait for a second
  digitalWrite(led, LOW);    // turn the LED off ,same as digitalWrite(10, LOW);
  delay(100);                       // wait for a second
}
