/*
 * Να γραφεί κώδικας, έτσι ώστε:
 * Να περιμένει το arduino να πατηθεί ένα button για να συνεχίσει.
 * Αφού πατηθεί το button, να πηγαίνει στη loop και να αυξάνει το pwm κύκλο του με
 * analogWrite() μέχρι έναν αριθμό (εδώ μέχρι το 80) και μετά ξανά από την αρχή, με
 * σκοπό να κάνει dimming ένα led.
 * 
 * Δηλαδή:
 * analogWrite(ledpin,1);
 * analogWrite(ledpin,2);
 * analogWrite(ledpin,3);...
 * 
 * Οι τιμές του analogWrite() να φαίνονται στη σειριακή.
 * 
 * Δηλαδή:
 * 1
 * 2
 * 3 ...
 * 
 * Να συζητηθεί η τεχνική low pass filter με RC
 */



//Pin variables
int led = 10; //led in pin 10
int button = 11; //button in pin 11

int buttonPress =1;
int ledVal = 0;

void setup() {
  Serial.begin(9600);
  //Pin Modes
  pinMode(led, OUTPUT); //led pin outputs power
  pinMode(button, INPUT); //button inputs signal
  
  //INIT
  digitalWrite(led, LOW); //initialize led with low

  Serial.println("press button to start program");
  while(buttonPress == 1){ //while button is not pressed, wait
    buttonPress = digitalRead(button);
  }
  //after button is pressed, continue the code
}

void loop() {
  //max ledVal value is 255
  analogWrite(led, ledVal); //write ledval pwm
  ledVal ++ ; //increase ledval
  
  Serial.println(ledVal); //Print ledval on serial
  
  if(ledVal == 40){ //if ledVal reaches 80, reset.
    ledVal =0;
  }
  
 delay(50);

}
